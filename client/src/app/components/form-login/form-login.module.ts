import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FormLoginComponent } from './form-login.component';

@NgModule({
    declarations: [FormLoginComponent],
    imports: [
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    exports: [FormLoginComponent]
})
export class FormLoginModule { }
