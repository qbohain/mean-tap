import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { UserModel } from '../../models/user.model';

@Component({
    selector: 'app-form-login',
    templateUrl: './form-login.component.html',
    styleUrls: ['./form-login.component.css']
})

export class FormLoginComponent implements OnInit, OnChanges {

    /**
     * Configuration
     */

    // Input / Output
    @Input() resetFormData: Boolean;
    @Output() sendFormData = new EventEmitter();

    // Declarations
    public form: FormGroup;
    public formData: UserModel;

    // Instance
    constructor( private FormBuilder: FormBuilder ) { }

    //

    /**
     * Methods
     */

    // Reset login form
    public resetForm = () => {

        // Set form validator to undefined
        this.form = this.FormBuilder.group({
            email: [undefined, Validators.required],
            password: [undefined, Validators.required]
        });

        // Set form datas to undefined
        this.formData = {
            email: undefined,
            password: undefined
        }

    }

    // Submit login form
    public submitForm = () => {

        // Set form datas from form values
        this.formData = {
            email: this.form.value.email,
            password: this.form.value.password,
        }
  
        // Emit form submition
        this.sendFormData.emit(this.formData);

    }

    //

    /**
     * Hooks
     */

    ngOnInit() {
        this.resetForm();
    }

    ngOnChanges(changes) {
        // Reset form data when user is logged in
        if(!changes.resetFormData.firstChange && changes.resetFormData.currentValue) {
            this.resetForm();
        };
    }

    //

}
