import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { UserModel } from '../../models/user.model';

@Component({
    selector: 'app-form-register',
    templateUrl: './form-register.component.html',
    styleUrls: ['./form-register.component.css']
})

export class FormRegisterComponent implements OnInit, OnChanges {

    /**
     * Configuration
     */

    // Input / Output
    @Input() resetFormData: Boolean;
    @Output() sendFormData = new EventEmitter();

    // Declarations
    public form: FormGroup;
    public formData: UserModel;
    public passwordError: Boolean = false;

    // Instance
    constructor( private FormBuilder: FormBuilder ) { }

    //

    /**
     * Methods
     */

    // Reset registration form
    public resetForm = () => {

        // Set form validator to undefined
        this.form = this.FormBuilder.group({
            first_name: [undefined, Validators.required],
            last_name: [undefined, Validators.required],
            email: [undefined, Validators.required],
            password: [undefined, Validators.required],
            valid_password: [undefined, Validators.required]
        });

        // Set form datas to undefined
        this.formData = {
            first_name: undefined,
            last_name: undefined,
            email: undefined,
            password: undefined,
            valid_password: undefined
        };

    }

    // Submit registration form
    public submitForm = () => {

        // Password and valid_password comparison
        if(this.form.value.password !== this.form.value.valid_password) { this.passwordError = true };

        // Set form datas from form values
        this.formData = {
            first_name: this.form.value.first_name,
            last_name: this.form.value.last_name,
            email: this.form.value.email,
            password: this.form.value.password
        }

        // Emit form submition
        this.sendFormData.emit(this.formData);

    }

    //

    /**
     * Hooks
     */

    ngOnInit() {
        this.resetForm();
    }

    ngOnChanges(changes) {
        // Reset form data when user is registrated
        if(!changes.resetFormData.firstChange && changes.resetFormData.currentValue) {
            this.resetForm();
        };
    }
    
    //

}
