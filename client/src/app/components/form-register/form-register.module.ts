import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FormRegisterComponent } from './form-register.component';

@NgModule({
    declarations: [FormRegisterComponent],
    imports: [
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    exports: [FormRegisterComponent]
})
export class FormRegisterModule { }
