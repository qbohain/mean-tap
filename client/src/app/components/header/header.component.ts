import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

import { AuthService } from '../../services/auth/auth.service';
import { HeaderService } from '../../services/header/header.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

    title: String;
    subtitle: String;
    status: String;

    constructor( private AuthService: AuthService, private HeaderService: HeaderService, private CookieService: CookieService, private Router: Router ) { }

    ngOnInit() {
        this.setHeader();
    }

    setHeader() {
        this.HeaderService.title.subscribe((newTitle) => {
            this.title = newTitle;
        });
        this.HeaderService.subtitle.subscribe((newSubtitle) => {
            this.subtitle = newSubtitle;
        });
        this.HeaderService.status.subscribe((newStatus) => {
            this.status = newStatus;
        });
    }

    logout() {
        this.CookieService.deleteAll('/');
        this.Router.navigateByUrl('/');
    }

}
