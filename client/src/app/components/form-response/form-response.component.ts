import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-form-response',
    templateUrl: './form-response.component.html',
    styleUrls: ['./form-response.component.css']
})

export class FormResponseComponent implements OnInit {

    /**
     * Configuration
     */

    // Input / Output
    @Input() content: String;
    @Output() close = new EventEmitter();
 
    // Instance
    constructor() { }

    //

    /**
     * Hooks
     */

    ngOnInit() {
    }

    //

}
