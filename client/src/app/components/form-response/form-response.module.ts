import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FormResponseComponent } from './form-response.component';

@NgModule({
    declarations: [FormResponseComponent],
    imports: [
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    exports: [FormResponseComponent]
})
export class FormResponseModule { }
