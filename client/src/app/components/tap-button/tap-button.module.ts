import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TapButtonComponent } from './tap-button.component';

@NgModule({
  declarations: [TapButtonComponent],
  imports: [
    CommonModule
  ]
})
export class TapButtonModule { }
