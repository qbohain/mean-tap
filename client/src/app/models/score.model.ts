export interface ScoreModel {
    score: Number,
    user_id?: String,
    user_fullname?: String,
}