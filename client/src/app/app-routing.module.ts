import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from './services/auth/auth.guard';

const routes: Routes = [
    {
        path: '',
        loadChildren: () => import('./routes/home-page/home-page.module').then(mod => mod.HomePageModule)
    },
    {
        path: 'me',
        loadChildren: () => import('./routes/me-page/me-page.module').then(mod => mod.MePageModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'tap',
        loadChildren: () => import('./routes/tap-page/tap-page.module').then(mod => mod.TapPageModule),
        canActivate: [AuthGuard]
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' })],
    exports: [RouterModule]
})
export class AppRoutingModule { }
