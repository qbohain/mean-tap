import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

import { HeaderService } from '../../services/header/header.service';
 
@Component({
    selector: 'app-tap-page',
    templateUrl: './tap-page.component.html',
    styleUrls: ['./tap-page.component.css']
})
export class TapPageComponent implements OnInit {

    constructor( private HeaderService: HeaderService, private CookieService: CookieService ) { }

    ngOnInit() {
        
    }

}
