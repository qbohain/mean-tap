import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TapPageComponent } from './tap-page.component';

const routes: Routes = [
    {
        path: '',
        component: TapPageComponent,
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TapPageRoutingModule { }
