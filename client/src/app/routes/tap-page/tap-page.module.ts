import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TapPageRoutingModule } from './tap-page-routing.module';
import { TapPageComponent } from './tap-page.component';

@NgModule({
  declarations: [TapPageComponent],
  imports: [
    CommonModule,
    TapPageRoutingModule
  ]
})
export class TapPageModule { }
