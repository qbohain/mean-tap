import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

import { UserModel } from '../../models/user.model';
import { ApiResponseModel } from '../../models/api-response.model';
import { AuthService } from '../../services/auth/auth.service';
import { HeaderService } from '../../services/header/header.service';

@Component({
    selector: 'app-home-page',
    templateUrl: './home-page.component.html',
    styleUrls: ['./home-page.component.css'],
})
export class HomePageComponent implements OnInit {

    /**
     * Configuration
     */

    // Registration form datas
    public resetFormDataRegister: Boolean = false;
    public displayReturnRegister: Boolean = false;
    public messageClassRegister: String;
    public apiMessageRegister: String;  

    // Login form datas
    public resetFormDataLogin: Boolean = false;
    public displayReturnLogin: Boolean = false;
    public messageClassLogin: String;
    public apiMessageLogin: String;

    // Instance
    constructor( private AuthService: AuthService, private HeaderService: HeaderService, private CookieService: CookieService, private Router: Router ) { }

    //

    /**
     * Methods
     */

    // Register new user
    public registerUser = (data: UserModel) => {

        // User data POST
        this.AuthService.register(data)
        .then((apiResponse: ApiResponseModel) => {

            // API Success Response
            this.messageClassRegister = 'success';
            this.apiMessageRegister = apiResponse.message;
            this.displayReturnRegister = true;
            // Reset form data
            this.resetFormDataRegister = true;

        }).catch((apiResponse: ApiResponseModel) => {

            // API Error Response
            this.messageClassRegister = 'error';
            this.apiMessageRegister = apiResponse.message;
            this.displayReturnRegister = true;

        });
    
    }

    // Connect user
    public loginUser = (data: UserModel) => {

        // User data POST
        this.AuthService.login(data)
        .then((apiResponse: ApiResponseModel) => {

            // API Success Response
            this.messageClassLogin = 'success';
            this.apiMessageLogin = apiResponse.message;
            this.displayReturnLogin = true;
            // Reset form data
            this.resetFormDataLogin = true;
            // Set login cookie
            this.CookieService.set('MEANTAP-token', apiResponse.data.token);
            this.CookieService.set('MEANTAP-fullname', `${apiResponse.data.user.first_name} ${apiResponse.data.user.last_name}`);
            this.CookieService.set('MEANTAP-user_id', apiResponse.data.user.user_id);
            // Redirect after login
            let redirect = this.AuthService.redirectUrl ? this.Router.parseUrl(this.AuthService.redirectUrl) : '/me';
            this.Router.navigateByUrl(redirect);

        }).catch((apiResponse: ApiResponseModel) => {

            // API Error Response
            this.messageClassLogin = 'error';
            this.apiMessageLogin = apiResponse.message;
            this.displayReturnLogin = true;

        });

    }

    //

    /**
     * Hooks
     */

    ngOnInit() {
        this.HeaderService.setTitle('MEAN TAP');
        this.HeaderService.setSubtitle('Login or register to play !');
        this.HeaderService.setStatus('home');
    }

    //

}
