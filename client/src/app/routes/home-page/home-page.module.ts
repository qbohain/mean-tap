import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomePageRoutingModule } from './home-page-routing.module';
import { HomePageComponent } from './home-page.component';
import { FormLoginModule } from '../../components/form-login/form-login.module';
import { FormRegisterModule } from '../../components/form-register/form-register.module';
import { FormResponseModule } from '../../components/form-response/form-response.module';

@NgModule({
    declarations: [HomePageComponent],
    imports: [
        CommonModule,
        HomePageRoutingModule,
        FormLoginModule,
        FormRegisterModule,
        FormResponseModule,
    ]
})
export class HomePageModule { }
