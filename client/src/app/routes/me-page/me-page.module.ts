import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MePageRoutingModule } from './me-page-routing.module';
import { MePageComponent } from './me-page.component';

@NgModule({
  declarations: [MePageComponent],
  imports: [
    CommonModule,
    MePageRoutingModule
  ]
})
export class MePageModule { }
