import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { interval } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';

import { ApiResponseModel } from '../../models/api-response.model';
import { HeaderService } from '../../services/header/header.service';
import { ScoreService } from '../../services/score/score.service';

@Component({
    selector: 'app-me-page',
    templateUrl: './me-page.component.html',
    styleUrls: ['./me-page.component.css']
})
export class MePageComponent implements OnInit {

    public tapCount: number = 0;

    fullname: String;
    seconds: number = 0;
    progressionPercentage: number = 0;
    isTapped: boolean = false;

    constructor( private CookieService: CookieService, private HeaderService: HeaderService, private ScoreService: ScoreService, private Router: Router ) { }

    userTap() {
        if(this.tapCount === 0) { this.startTimer(10) }
        this.isTapped = !this.isTapped;
        this.tapCount++;
    }

    startTimer(duration: number) {
        // Timer creation
        const timer$ = interval(1000);
        const timerSubscription = timer$.subscribe(currentSec => {

            this.seconds = currentSec;
            this.progressionPercentage = currentSec * 10;

            console.log(`currentSec : ${currentSec}, duration : ${duration}`);

            // If timer == duration
            if(currentSec === duration) {

                timerSubscription.unsubscribe();

                this.ScoreService.postScore({score: this.tapCount})
                .then((apiResponse: ApiResponseModel) => {
                    console.log(apiResponse);
                    this.Router.navigateByUrl('/tap');
                })
                .catch((apiResponse: ApiResponseModel) => {
                    console.log(`Erreur : ${apiResponse}`);
                });
            }

        });

    }

    getFullname() {
        this.fullname = this.CookieService.get('MEANTAP-fullname');
    }

    ngOnInit() {
        this.getFullname();
        this.HeaderService.setTitle(`Hello ${this.fullname}`);
        this.HeaderService.setSubtitle('You have 10 seconds to tap ! Ready ?');
        this.HeaderService.setStatus('playing');
    }

}
