import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class HeaderService {

    title = new BehaviorSubject('Title');
    subtitle = new BehaviorSubject('Subtitle');
    status = new BehaviorSubject('Status');

    constructor() { }

    setTitle(title: string) {
        this.title.next(title);
    }

    setSubtitle(subtitle: string) {
        this.subtitle.next(subtitle);
    }

    setStatus(status: string) {
        this.status.next(status);
    }

}
