import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';

import { environment } from '../../../environments/environment';
import { ScoreModel } from '../../models/score.model';

@Injectable({
    providedIn: 'root'
})
export class ScoreService {

    // HttpClient and CookieService module injection
    constructor( private HttpClient: HttpClient, private CookieService: CookieService ) { }

    // Post current player score
    public postScore(scoreData: ScoreModel): Promise<any> {

        // Headers configuration
        let headers = this.generateHeaders();

        // POST 'scores'
        return this.HttpClient.post(`${environment.apiUrl}/scores`, scoreData, { headers })
        .toPromise().then(this.getData).catch(this.handleError);

    }

    // Get all scores
    public getScores(): Promise<any> {

        // Headers configuration
        let headers = this.generateHeaders();

        // GET 'scores'
        return this.HttpClient.get(`${environment.apiUrl}/scores`, { headers })
        .toPromise().then(this.getData).catch(this.handleError);

    }

    // Headers configuration
    private generateHeaders() {
        let userToken = this.CookieService.get('MEANTAP-token');
        let headers = new HttpHeaders();
        headers.append('Content-type', 'application/json');
        headers.append('Authorization', userToken);
        return headers;
    }

    // Get the API response
    private getData(res: any) {
        return res || {};
    };
  
    // Get the API error
    private handleError(err: any) {
        return Promise.reject(err.error);
    };
    
}
