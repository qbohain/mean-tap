import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';

import { environment } from '../../../environments/environment';
import { UserModel } from '../../models/user.model';

@Injectable({
    providedIn: 'root'    
})
export class AuthService {

    // Redirect url
    redirectUrl: string;
    
    // HttpClient and CookieService module injection
    constructor( private HttpClient: HttpClient, private CookieService: CookieService ) { }

    // User register method
    public register(userData: UserModel): Promise<any> {

        // Headers configuration
        let headers = this.generateHeaders();

        // POST 'auth/register'
        return this.HttpClient.post(`${environment.apiUrl}/auth/register`, userData, { headers })
        .toPromise().then(this.getData).catch(this.handleError);
    
    }

    // User login method
    public login(userData: UserModel): Promise<any> {

        // Headers configuration
        let headers = this.generateHeaders();

        // POST 'auth/login'
        return this.HttpClient.post(`${environment.apiUrl}/auth/login`, userData, { headers })
        .toPromise().then(this.getData).catch(this.handleError);

    }

    // Get the user ID
    public getUserId(): boolean {
        // Check if token exists
        return this.CookieService.check('MEANTAP-token');
    };

    // Headers configuration
    private generateHeaders() {
        let headers = new HttpHeaders();
        headers.append('Content-type', 'application/json');
        return headers;
    }

    // Get the API response
    private getData(res: any) {
        return res || {};
    };
  
    // Get the API error
    private handleError(err: any) {
        return Promise.reject(err.error);
    };

}
