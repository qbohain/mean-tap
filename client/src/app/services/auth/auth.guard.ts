import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

import { AuthService } from './auth.service';

@Injectable({
    providedIn: 'root',
})

export class AuthGuard implements CanActivate {

    constructor(
        private AuthService: AuthService,
        private Router: Router,
    ) {}

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

        if(this.AuthService.getUserId()) { return true }

        this.AuthService.redirectUrl = state.url;
        this.Router.navigateByUrl('/');
        return false;

    }

}