/*
Imports
*/
// NodeJS
const { Router } = require('express');
const passport = require('passport');
// Inner
const { setAuthentication } = require('../services/authentication.service');
setAuthentication(passport);
const AuthRouterClass = require('./auth/auth.routes');
const ScoreRouterClass = require('./scores/scores.routes');
const FrontRouterClass = require('./front/front.routes');
//

/*
Routers definition
*/
// Parent
const mainRouter = Router();
const apiRouter = Router();
const frontRouter = new FrontRouterClass({ passport });
// Childs
const authRouter = new AuthRouterClass({ passport });
const scoreRouter = new ScoreRouterClass({ passport });
//

/*
Routes configuration
*/
// Main
mainRouter.use('/api', apiRouter);
mainRouter.use('/', frontRouter.routerInit());
// API
apiRouter.use('/auth', authRouter.routerInit());
apiRouter.use('/scores', scoreRouter.routerInit());
//

/*
Export
*/
module.exports = { mainRouter };
//