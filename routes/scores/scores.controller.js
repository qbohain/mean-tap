/*
Imports
*/
const ScoreModel = require('../../models').score;
const { fetchSingle, findAll } = require('../main.controller');
//

/*
Methods
*/

/**
 * Score creation method
 * 
 * @param {Object} body: Object => score: Int
 * @param {String} user_id: String => Current user ID
 * 
 * @return {Promise} Returns promise with mongo response
 */
const createScore = (body, user_id) => {

    return new Promise((resolve, reject) => {

        body.user_id = user_id;

        ScoreModel.create(body)
        .then(mongoResponse => resolve(mongoResponse))
        .catch(mongoResponse => reject(mongoResponse));

    });

}
//

/**
 * Scores read method
 * 
 * @return {Promise} Returns promise with mongo response
 */
const readScores = () => {
    return findAll('score');
}
//

/**
 * Score read by ID method
 * 
 * @param {String} score_id: String => Current user ID
 * 
 * @return {Promise} Returns promise with mongo response
 */
const readScore = score_id => {
    return fetchSingle(score_id, 'score');
}
//

/**
 * Score update by ID method
 * 
 * @param {Object} body: Object => score: Int
 * @param {String} score_id: String => Current user ID
 * 
 * @return {Promise} Returns promise with mongo response
 */
const updateScore = (body, score_id) => {

}
//

/**
 * Score delete by ID method
 * 
 * @param {String} score_id: String => Current user ID
 * 
 * @return {Promise} Returns promise with mongo response
 */
const deleteScore = score_id => {

}
//

//

/*
Export
*/
module.exports = { createScore, readScores, readScore, updateScore, deleteScore };
//