/*
Imports
*/
// Node
const express = require('express');
const scoreRouter = express.Router();
// Inner
const scoreMandatories = require('../../services/mandatory.service').score;
const Vocabulary = require('../../services/vocabulary.service');
const { sendBodyError, sendFieldsError, sendApiSuccessResponse, sendApiErrorResponse } = require('../../services/response.service');
const { checkFields } = require('../../services/request.service');
const { createScore, readScores, readScore, updateScore, deleteScore } = require('./scores.controller');
//

/*
Routes definition
*/
class ScoreRouterClass {

    // Passport injection
    constructor({ passport }) {
        this.passport = passport;
    }

    // Routes creation
    routes() {

        // Read all stored scores
        scoreRouter.get('/', this.passport.authenticate('jwt', { session: false }), (req, res) => {

            // Scores read
            readScores()
            .then(apiResponse => sendApiSuccessResponse(res, Vocabulary.request.success, apiResponse))
            .catch(apiResponse => sendApiErrorResponse(res, Vocabulary.request.failed, apiResponse));

        });

        // Read stored score by ID
        scoreRouter.get('/:score_id', this.passport.authenticate('jwt', { session: false }), (req, res) => {

            // If empty params
            if(!req.params || !req.params.score_id) sendBodyError(res, Vocabulary.errors.emptyParams);
            // Score read by ID
            readScore(req.params.score_id)
            .then(apiResponse => sendApiSuccessResponse(res, Vocabulary.request.success, apiResponse))
            .catch(apiResponse => sendApiErrorResponse(res, Vocabulary.request.failed, apiResponse));

        });

        // Create score
        scoreRouter.post('/', this.passport.authenticate('jwt', { session: false }), (req, res) => {

            // If empty body
            if(typeof req.body === 'undefined' || req.body === null) sendBodyError(res, Vocabulary.errors.emptyBody);
            // Checking request's fields
            const { missing, extra, validFields } = checkFields(scoreMandatories.create, req.body);
            // Fields errors management
            if(!validFields) sendFieldsError(res, Vocabulary.errors.badFields, missing, extra);
            // Score creation
            else {
                createScore(req.body, req.user.id)
                .then(apiResponse => sendApiSuccessResponse(res, Vocabulary.request.success, apiResponse))
                .catch(apiResponse => sendApiErrorResponse(res, Vocabulary.request.failed, apiResponse));
            }

        });
 
        // Update score by ID
        scoreRouter.put('/:score_id', this.passport.authenticate('jwt', { session: false }), (req, res) => {

            // If empty params
            if(!req.params || !req.params.score_id) sendBodyError(res, Vocabulary.errors.emptyParams);
            // If empty body
            if(typeof req.body === 'undefined' || req.body === null) sendBodyError(res, Vocabulary.errors.emptyBody);
            // Checking request's fields
            const { missing, extra, validFields } = checkFields(scoreMandatories.update, req.body);
            // Fields errors management
            if(!validFields) sendFieldsError(res, Vocabulary.errors.badFields, missing, extra);
            // Score update
            else {
                updateScore(req.body, req.user.id)
                .then(apiResponse => sendApiSuccessResponse(res, Vocabulary.request.success, apiResponse))
                .catch(apiResponse => sendApiErrorResponse(res, Vocabulary.request.failed, apiResponse));
            }

        });

        // Delete score by ID
        scoreRouter.delete('/:score_id', this.passport.authenticate('jwt', { session: false }), (req, res) => {

            // If empty params
            if(!req.params || !req.params.score_id) sendBodyError(res, Vocabulary.errors.emptyParams);
            // Score delete
            deleteScore(req.params.score_id)
            .then(apiResponse => sendApiSuccessResponse(res, Vocabulary.request.success, apiResponse))
            .catch(apiResponse => sendApiErrorResponse(res, Vocabulary.request.failed, apiResponse));

        });

    }

    // Router init method
    routerInit() {
        this.routes();
        return scoreRouter;
    }

}
//

/*
Exports
*/
module.exports = ScoreRouterClass;
//