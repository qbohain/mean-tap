/*
Imports
*/
// Node
const express = require('express');
const authRouter = express.Router();
// Inner
const authMandatories = require('../../services/mandatory.service').auth;
const Vocabulary = require('../../services/vocabulary.service');
const { sendBodyError, sendFieldsError, sendApiSuccessResponse, sendApiErrorResponse } = require('../../services/response.service');
const { checkFields } = require('../../services/request.service');
const { userRegistration, userLogin } = require('./auth.controller');
//

/*
Routes definition
*/
class AuthRouterClass {

    // Passport injection
    constructor({ passport }) {
        this.passport = passport;
    }

    // Routes creation
    routes() {

        // User registration route
        authRouter.post('/register', (req, res) => {

            // If empty body
            if(typeof req.body === 'undefined' || req.body === null) sendBodyError(res, Vocabulary.errors.emptyBody);

            // Checking request's fields
            const { missing, extra, validFields } = checkFields(authMandatories.register, req.body);
            // Fields errors management
            if (!validFields) sendFieldsError(res, Vocabulary.errors.badFields, missing, extra)
            // User registration
            else {
                userRegistration(req.body)
                .then(apiResponse => sendApiSuccessResponse(res, Vocabulary.request.success, apiResponse))
                .catch(apiResponse => sendApiErrorResponse(res, Vocabulary.request.failed, apiResponse));
            }

        });

        // User login route
        authRouter.post('/login', (req, res) => {

            // If empty body
            if(typeof req.body === 'undefined' || req.body === null) sendBodyError(res, Vocabulary.errors.emptyBody);

            // Checking request's fields
            const { missing, extra, validFields } = checkFields(authMandatories.login, req.body);
            // Fields errors management
            if (!validFields) sendFieldsError(res, Vocabulary.errors.badFields, missing, extra)
            // User login
            else {
                userLogin(req.body)
                .then(apiResponse => sendApiSuccessResponse(res, Vocabulary.request.success, apiResponse))
                .catch(apiResponse => sendApiErrorResponse(res, Vocabulary.request.failed, apiResponse));
            }

        });

        authRouter.get('/', this.passport.authenticate('jwt', { session: false }), (req, res) => {
            return sendApiSuccessResponse(res, Vocabulary.request.success, { _id: req.user._id });
        });

    }

    // Router init method
    routerInit() {
        this.routes();
        return authRouter;
    }

}
//

/*
Export
*/
module.exports = AuthRouterClass;
//