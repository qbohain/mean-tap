/*
Imports
*/
// NodeJS
const bcrypt = require('bcryptjs');
// Inner
const UserModel = require('../../models').user;
//

/*
Methods
*/

/**
 * User registration method
 * 
 * @param {Object} body: Object => email: String (unique), password: String
 * 
 * @return {Promise} Returns promise with mongo response
 */
const userRegistration = body => {

    return new Promise((resolve, reject) => {

        UserModel.findOne({ email: body.email }, (error, user) => {
            // If error occurs
            if(error) return reject(error)
            // If user exists
            else if(user) return reject('User already exists')
            // Registration
            else {
                // Hashing user's password
                bcrypt.hash(body.password, 10)
                .then(hashedPassword => {
                    // Replacing with hashed password
                    body.password = hashedPassword;
                    // User creation
                    UserModel.create(body)
                    .then(mongoResponse => resolve(mongoResponse))
                    .catch(mongoResponse => reject(mongoResponse));
                })
                .catch(hashError => reject(hashError));
            }
        });

    });

}
//

/**
 * User login method
 * 
 * @param {Object} body: Object => email: String, password: String
 * 
 * @return {Promise} Returns promise with mongo response: logged user on success | error on invalid credentials
 */
const userLogin = body => {

    return new Promise((resolve, reject) => {

        UserModel.findOne({ email: body.email}, (error, user) => {
            // If error occurs
            if(error) return reject(error)
            // If user doesn't exist
            else if(!user) return reject('Unknow user')
            // Checking password
            else {
                const validPassword = bcrypt.compareSync(body.password, user.password);
                if(!validPassword) reject('Incorrect password');
                else {
                    resolve({user, token: user.generateJwt()}); 
                } 
            }
        });

    });

}
//

/*
Export
*/
module.exports = { userRegistration, userLogin };
//