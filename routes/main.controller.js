/*
Imports
*/
const Models = require('../models/index');
const Vocabulary = require('../services/vocabulary.service');
//

/*
Methods
*/

/**
 * Create new object if it doesn't exist
 * 
 * @param {Request} req: Request => The client request
 * @param {String} model: String => The document model
 * @param {Object} requestOptions: Object => Existing item's options
 * 
 * @return {Promise} Returns promise with mongo response
*/
const findOneRejectOrCreate = (req, model, requestOptions) => {

    return new Promise((resolve, reject) => {

        // Check if item already exist
        Models[model].findOne(requestOptions, (error, item) => {
            // Request error
            if(error) { return reject(error) }
            else if(item) { return reject(Vocabulary.errors.notNew) }
            else {
                // Save item in DB
                Models[model].create(req.body)
                .then( response => resolve(response))
                .catch( response => reject(response));
            };
        });

    });
    
};

/**
 * Push item into a document element
 * 
 * @param {String} _id: String => The ID of the object to update
 * @param {String} model: String => The document model
 * @param {Object} dataSet: Object => Data to push
 * 
 * @return {Promise} Returns promise with mongo response
*/
const findOneAndPushItem = (_id, model, dataSet) => {

    return new Promise((resolve, reject) => {

        // Fetch model by id
        return Models[model].findOneAndUpdate({ _id: _id }, { $addToSet: dataSet }, { 'new': true } , (error, item) => {
            // Request error
            if(error) { return reject(error) }
            // Undefined item
            else if (item === null) { return reject('Undefined item') }
            // Success return
            else { return resolve(item) };
        });

    });

};

/**
 * Add item into a document
 * 
 * @param {String} _id: String => The ID of the object to update
 * @param {String} model: String => The document model
 * @param {Object} dataSet: Object => Data to add
 * 
 * @return {Promise} Returns promise with mongo response
*/
const findOneAndAddItem = (_id, model, dataSet) => {

    return new Promise((resolve, reject) => {

        // Fetch model by id
        Models[model].findOneAndUpdate({ _id: _id }, { $set: dataSet }, { 'new': true } , (error, item) => {
            // Request error
            if(error) { return reject(error) }
            else { return resolve(item) };
        });
    
    });

};

/**
 * Remove item of a document or document element
 * 
 * @param {String} _id: String => The ID of the object to update
 * @param {String} model: String => The document model
 * @param {Object} dataSet: Object => Data to remove
 * 
 * @return {Promise} Returns promise with mongo response
*/
const findOneAndRemoveItem = (_id, model, dataSet) => {

    return new Promise((resolve, reject) => {

        // Fetch model by id
        Models[model].findOneAndUpdate({ _id: _id }, { $pull: dataSet }, { 'new': true } , (error, item) => {
            // Request error
            if(error) { return reject(error) }
            else { return resolve(item) };
        });

    });

} 

/**
 * Delete object if exist & if user is allowed
 * 
 * @param {String} _id: String => The ID of the object to delete
 * @param {String} model: String => The document model
 * @param {String} author: String => The user ID from JWT
 * 
 * @return {Promise} Returns promise with mongo response
*/
const findOneAndDelete = (_id, model, author) => {

    return new Promise((resolve, reject) => {
        
        // Fecth model by id
        Models[model].findById(_id , (error, item) => {
            // Request error
            if(error) { return reject(error) }
            else if( item.author !== author ) { return reject(Vocabulary.errors.notAllowed) }
            else { return resolve(item) };
        });

    });

};

/**
 * Get informations from object ID
 * 
 * @param {String} _id: String => The ID of the object
 * @param {String} model: String => The document model
 * 
 * @return {Promise} Returns promise with mongo response
*/
const fetchSingle = (_id, model) => {

    return new Promise((resolve, reject) => {

        // Fecth model by id
        Models[model].findById(_id, (error, item) => {
            // Request error
            if(error) { return reject(error) }
            else { return resolve(item) };
        });     

    });

};

/**
 * Get informations from array of object ID
 * 
 * @param {Array<String>} _idCollection: String[] => The array of object ID
 * @param {String} model: String => The document model
 * 
 * @return {Promise} Returns promise with mongo response
*/
const fetchAll = (_idCollection, model) => {

    return new Promise((resolve, reject) => {
        
        // Set empty collection
        let dataArray = [];

        // Fetch _id collection
        ((async function loop() {
            for(let i = 0; i < _idCollection.length; ++i) {
                const item = await fetchSingle(_idCollection[i], model);
                dataArray.push(item)
            }
            return resolve(dataArray);
        })());

    });

};

/**
 * Get all items from collection model
 * 
 * @param {String} model: String => The document model
 * @param {Object} requestOptions: Object => Request option(s) for find() parameters
 * 
 * @return {Promise} Returns promise with mongo response
*/
const findAll = (model, requestOptions = null) => {

    return new Promise((resolve, reject) => {

        if(requestOptions === null) {
            // Fecth model by id
            Models[model].find((error, item) => {
                // Request error
                if(error) { return reject(error) }
                else { return resolve(item) };
            });
        }
        else {
            // Fetch model by request option(s)
            Models[model].find(requestOptions, (error, item) => {
                // Request error
                if(error) { return reject(error) }
                else { return resolve(item) };
            });
        }

    });

};

//

/*
Export
*/
module.exports = { findOneRejectOrCreate, findOneAndPushItem, findOneAndAddItem, findOneAndRemoveItem, findOneAndDelete, fetchSingle, fetchAll, findAll };
//