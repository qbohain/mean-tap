/*
Imports
*/
// Node
const express = require('express');
const frontRouter = express.Router();
//

/*
Routes definition
*/
class FrontRouterClass {

    constructor({ passport }) {
        this.passport = passport;
    }

    routes() {

        frontRouter.get('/*', (req, res) => {
            res.render('index');
        });

    }

    routerInit() {
        this.routes();
        return frontRouter;
    }

}
//

/*
Exports
*/
module.exports = FrontRouterClass;
//