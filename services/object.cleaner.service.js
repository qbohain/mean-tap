/*
Service Definition
*/

/**
 * Clean up an object with defined filters
 * 
 * @param {Object} object: Object => Object to clean up 
 * @param {Array<String>} filters: String[] => Array of filters to apply 
 * 
 * @return {Object} Returns cleaned object
 */
const cleanObject = (object, filters) => {
    let cleanedObject = {};
    for(const prop in object) {
        if(filters.indexOf(prop) >= 0) cleanedObject[prop] = object[prop];
    }
    return cleanedObject;
}
//

/**
 * Clean up an array of objects with defined filters
 * 
 * @param {Array<Object>} objectArray: Object[] => Array of objects to clean up 
 * @param {Array<String>} filters: String[] => Array of filters to apply 
 * 
 * @return {Array<Object>} Returns cleaned objects array
 */
const cleanObjectsArray = (objectArray, filters) => {
    let cleanedArray = [];
    for(let i = 0 ; i < objectArray.length ; i++) {
        cleanedArray.push(cleanObject(objectArray[i], filters));
    }
    return cleanedArray;
}
//

/*
Export
*/
module.exports = { cleanObject, cleanObjectsArray };
//