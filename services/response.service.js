/*
Service definition
*/

// Request Body Error Response
const sendBodyError = (response, errorMessage) => {
    return response.status(400).json({
        message: errorMessage,
        err: null,
        data: null
    });
}

// Request Fields Error Response
const sendFieldsError = (response, errorMessage, missing, extra) => {
    return response.status(400).json({
        message: errorMessage,
        err: { missing, extra },
        data: null  
    });
}

// API Success Reponse
const sendApiSuccessResponse = (response, successMessage, data) => {
    return response.status(200).send({
        message: successMessage,
        err: null,
        data: data
    });
}

// API Error Response
const sendApiErrorResponse = (response, errorMessage, error) => {
    return response.status(500).json({
        message: errorMessage,
        error,
        data: null
    });
}

//

/*
Export
*/
module.exports = { sendBodyError, sendFieldsError, sendApiSuccessResponse, sendApiErrorResponse };
//