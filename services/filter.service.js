/*
Service Definition
*/
const Filters = {
    auth: [],
    friends: {
        getAll: ['username', 'first_name', 'last_name']
    },
    groups: {

    },
    events: {

    },
}
//

/*
Export
*/
module.exports = Filters;
//