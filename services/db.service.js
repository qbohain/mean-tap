/*
Imports
*/
const mongoose = require('mongoose');
//

/*
Service Definition
*/
const initClient = () => {
    return new Promise((resolve, reject) => {
        mongoose.connect(process.env.MONGO_URL, { useNewUrlParser: true, useFindAndModify: false })
        .then(db => resolve(process.env.MONGO_URL))
        .catch(err => reject(`MongoDB not connected`, err));
    });
}
//

/*
Exports
*/
module.exports = { initClient };
//