/*
Service Definition
*/
const Mandatories = {
    auth: {
        login: ['email', 'password'],
        register: ['email', 'password', 'first_name', 'last_name'],
    },
    score: {
        create: ['score'],
        update: ['score'],
    }
}
//

/*
Export
*/
module.exports = Mandatories;
//