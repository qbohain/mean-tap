/*
Service Definition
*/

// Fields checking
const checkFields = (required, object) => {
    
    // Wrong fields storing
    const missing = [];
    const extra = [];

    // Missing fields checking
    required.forEach(field => {
        if(!(field in object)) missing.push(field);
    });

    // Extra fields checking
    for(const field in object) {
        if(required.indexOf(field) === -1) extra.push(field);
    }

    // Setting service state response
    const validFields = (missing.length === 0 && extra.length === 0);

    // Returning variables
    return { validFields, missing, extra };

}

// Optional fields checking
const checkOptionalFields = (optional, object) => {

    // Wrong fields storing
    const extra = [];

    // Extra fields checking
    for(const field in object) {
        if(optional.indexOf(field) === -1) extra.push(field);
    }

    const validFields = (extra.length === 0);

    return { validFields, extra };

}

//

/*
Export
*/
module.exports = { checkFields, checkOptionalFields };
//