/*
Service Definition
*/
const Vocabulary = {

    // Request messages
    request: {
        success: 'Request succeed',
        failed: 'Request failed'
    },

    // Errors messages
    errors: {
        // Fields errors 
        emptyBody: 'No data provided in body',
        emptyParams: 'No data provided in params',
        badFields: 'Bad fields provided',
    },

}
//

/*
Export
*/
module.exports = Vocabulary;
//