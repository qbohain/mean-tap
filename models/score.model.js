/*
Imports
*/
const mongoose = require('mongoose');
const { Schema } = mongoose;
//

/*
Schema Definition
*/
const scoreSchema = new Schema({
    score: Number,
    user_id: String,
}, { timestamps: true });
//

/*
Exports
*/
const ScoreModel = mongoose.model('score', scoreSchema);
module.exports = ScoreModel;
//