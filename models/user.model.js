/*
Imports
*/
// Mongoose
const mongoose = require('mongoose');
const { Schema } = mongoose;
// JWT
const jwt = require('jsonwebtoken');
//

/*
Schema Definition
*/
const userSchema = new Schema({
    first_name: String,
    last_name: String,
    email: String,
    password: String,
}, { timestamps: true });
//

/*
Methods
*/
userSchema.methods.generateJwt = function generateJwt(){

    // JWT expiration's settings
    const expiry = new Date();
    expiry.setDate(expiry.getDate() + 59); // expirates in 60 days

    /**
     * JWT Creation method
     * 
     * @param {Object} object: Object => informations needed for the access token creation
     * @param {String} JWT_SECRET: String => hash's secure key for the token hash (cf. '.env')
     * 
     * @return {String} Returns the JSON Web Token string
     */
    return jwt.sign({
        _id: this._id,
        email: this.email,
        password: this.password,
        expireIn: '10s',
        exp: parseInt(expiry.getTime() / 100, 10)
    }, process.env.JWT_SECRET);

}
//

/*
Exports
*/
const UserModel = mongoose.model('user', userSchema);
module.exports = UserModel;
//