/*
Models definition
*/
const Models = {
    user: require('./user.model'),
    score: require('./score.model'),
};
//

/*
Export
*/
module.exports = Models;
//