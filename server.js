/*
Imports
*/
// NodeJS
const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const path = require('path');
const ejs = require('ejs');
require('dotenv').config();
// Inner
const mongoDB = require('./services/db.service');
const { mainRouter } = require('./routes/main.router');
//

/*
Server Definition
*/
const server = express();
const port = process.env.PORT;
//

/*
Server Configuration
*/
class ServerClass {

    // Server initialisation
    serverInit() {

        // Views configuration
        server.engine( 'html', ejs.renderFile );
        server.set( 'view engine', 'html' );
        server.set( 'views', __dirname + '/www' );
        server.use( express.static(path.join(__dirname, 'www')) );

        // Body-parser definition
        server.use(bodyParser.json({limit: '10mb'}));
        server.use(bodyParser.urlencoded({ extended: true }));
        // Cookie-parser definition
        server.use(cookieParser(process.env.COOKIE_SECRET));

        // Main router definiton
        server.use('/', mainRouter);

        // Running server
        this.serverLaunch();

    }

    // Server launching
    serverLaunch() {

        mongoDB.initClient()
        .then(mongooseResponse => {
            server.listen(port, () => console.log({ database: mongooseResponse, server: `http://localhost:${port}` }))
        })
        .catch(mongooseError => console.log(mongooseError));

    }

}
//

/*
Starting server
*/
new ServerClass().serverInit();
//